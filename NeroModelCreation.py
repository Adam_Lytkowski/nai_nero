import matplotlib.pyplot as plt
import cv2

from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Activation,Dropout,Flatten,Conv2D,MaxPooling2D,Dense


#Konwerter obrazów
image_gen = ImageDataGenerator(rotation_range=30, #rotacja obrazka o 30 stopni
                              width_shift_range=0.1, #rozciaganie obrazka, maksymalnie o 10%
                              height_shift_range=0.1, #jak wyżej, wysokość
                              rescale=1/255, #normalizacja obrazka
                              shear_range=0.2, #wycięcie części obrazka, max 20%
                              zoom_range=0.2, #zoomowanie 20%
                              horizontal_flip=True, #Dozwolony horyzontal flip
                              fill_mode='nearest') #wypelnij brakujące pixele wartością najbliższego pixela

#Stworzenie modelu--------------------------------------------------------------
model = Sequential()

# warstwa konwolucyjna -Convolutional Layer
model.add(Conv2D(filters=32,kernel_size=(3,3),input_shape=(150,150,3),activation='relu'))
model.add(MaxPooling2D(pool_size=(2,2)))

model.add(Conv2D(filters=64,kernel_size=(3,3),input_shape=(150,150,3),activation='relu'))
model.add(MaxPooling2D(pool_size=(2,2)))

model.add(Conv2D(filters=64,kernel_size=(3,3),input_shape=(150,150,3),activation='relu'))
model.add(MaxPooling2D(pool_size=(2,2)))

#spłaszczenie obrazka do 1 wymiaru 2D---->1D
model.add(Flatten())

model.add(Dense(128))
model.add(Activation('relu'))

#Losowe wyłączenie 50% neuronów
model.add(Dropout(0.5))

#Ostatnia warstwa, jedynka bo sa dwie mozliwosci Nero albo Pies(inny)
model.add(Dense(1))
model.add(Activation('sigmoid'))

model.compile(loss='binary_crossentropy',
             optimizer='adam',
             metrics=['accuracy'])
#------------------------------------------------------------------------------------

batch_size=16
input_shape= (150,150,3)
#Przygotowanie obrazów do trenowania i testowych
train_image_gen = image_gen.flow_from_directory('train',
                                               target_size=input_shape[:2],
                                               batch_size=batch_size,
                                               class_mode='binary')

test_image_gen = image_gen.flow_from_directory('test',
                                               target_size=input_shape[:2],
                                               batch_size=batch_size,
                                               class_mode='binary')
#Trenowanie modelu  1 "epoch" to 16 losowo wybranych obrazow razy 15
results = model.fit_generator(train_image_gen,epochs=150,steps_per_epoch=15,
                             validation_data=test_image_gen,validation_steps=12)

model.save('CzyNero.h5')