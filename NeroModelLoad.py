import matplotlib.pyplot as plt
import cv2
import time
import numpy as np

from keras.models import load_model
from keras.preprocessing import image


model = load_model('CzyNero.h5')
    
nero_cascade = cv2.CascadeClassifier('nero-detector.xml')

#funkcja, ktora uzywa zaladowanej kaskady nero_cascade do narysowania prostokatu gdy wykryje Nera
def detect_nero(img):
    nero_img = img.copy()
    nero_rects = nero_cascade.detectMultiScale(nero_img,scaleFactor=1.3, minNeighbors=3, minSize=(100,100)) 
    
    for (x,y,w,h) in nero_rects: 
        cv2.rectangle(nero_img, (x,y), (x+w,y+h), (0,255,0), 4) 
        
    return nero_img

#funkcja, ktora konwertuje obrazek do formatu oczekiwanego przez model
def convert_file(file):
    img = cv2.resize(file,(150,150),interpolation=cv2.INTER_CUBIC)
    img = np.array(img, dtype='float32')
    img = img/255
    img = np.expand_dims(img,axis=0)
    
    return img

#funkcja, ktora sprawdza czy wg modelu na zdjeciu jest Nero. Jesli zwraca 1, to tak.
def check_dog(img):
    check = model.predict_classes(img)
    if check.item(0)==1:
        return 1
    else:
        return 0
    

# "uchwycenie" wideo
cap = cv2.VideoCapture('videos/nero-video9.mp4')
    
while True:
    #przechwytywanie klatka po klatce
    ret, frame = cap.read()
    
    #kopiujemy klatke do zmiennej img
    img = frame.copy()
    #konwertujemy klatke do formatu oczekiwanego przez model
    img = convert_file(img)
    font = cv2.FONT_HERSHEY_SIMPLEX
    prediction_prob = model.predict(img)
    
    #Sprawdzenie czy wg modelu Nero miesci sie w klatce
    if check_dog(img)==1:
        cv2.putText(frame,text='Nero wykryty na:',org=(0,50),fontFace=font,fontScale= 1,color=(0,255,0),thickness=2,lineType=cv2.LINE_AA)
        cv2.putText(frame,text=f'{prediction_prob}',org=(0,100),fontFace=font,fontScale= 1,color=(0,255,0),thickness=2,lineType=cv2.LINE_AA)
    else:
        cv2.putText(frame,text='Brak Nera',org=(0,100),fontFace=font,fontScale= 1,color=(0,0,255),thickness=2,lineType=cv2.LINE_AA)
    
    #Sprawdzenie czy Nero jest w klatce uzywajac kaskady nero-detector
    frame = detect_nero(frame)
    
    # wyswietlenie przerobionej klatki
    cv2.imshow('frame',frame)
    
    # Jesli wideo sie skonczy albo zostanie nacisniety ESC, okno sie zamknie
    if cv2.waitKey(1) & 0xFF == 27:
        break
        
cap.release()
cv2.destroyAllWindows()

